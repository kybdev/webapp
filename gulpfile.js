	/*
	AUTHOR: ryan.jeric
	USAGE: FOR PAGE OPTIMIZATION. Will solve GTmetrix MInify js issue
        NOTE: No cats were harmed in the making of this code 
	------------------
	GULP GUIDE : http://codehangar.io/getting-started-gulp-task-runner-tutorial/
	SOURCE: http://codehangar.io/concatenate-and-minify-javascript-with-gulp/ + https://www.npmjs.com/package/gulp-clean-css
	------------------
	Installation:
	1. In the root of your project on Terminal "npm init" - this will generate new package.json.
	2. "npm install gulp-concat gulp-rename gulp-uglify gulp-sequence --save-dev --save" 
	 it will update your package.json and it will create new folder name npm_modules and will start download dependencies

	You should see this in your package.json
	-----------------------
	"devDependencies": {
    		"gulp-concat": "^2.6.1",
   		  "gulp-rename": "^1.2.2",
    		"gulp-sequence": "^0.4.6",
    		"gulp-uglify": "^3.0.0"
  }
  	-----------------------
  	3. RUN terminal -> gulp fe-minify-all
  	*/
  	var gulp = require('gulp');
  	var concat = require('gulp-concat');
  	var rename = require('gulp-rename');
  	var uglify = require('gulp-uglify');
  	var gulpSequence = require('gulp-sequence');

  	/*---------MINIFY EVERYTHING IN FRONTEND JS ---------*/
    gulp.task('default', function(){
      console.log("Run: gulp fe-minify-all");
    });
  	gulp.task('fe-scripts', function () {
  		return gulp.src([
  			'public/fe/scripts/**/*.js',
  			'!public/fe/scripts/**/*.min.js', //to exclude already minified js
  			])
  		.pipe(uglify({ mangle: false }).on('error', function(e){ // Mangle set to false so that angular js variables will not be uglified.
  			console.log(e);
  		}))
  		.pipe(rename(function(path) {
  			path.extname = ".js";
  		}))
  		.pipe(gulp.dest('public/fe/min_scripts')); // destination folder of the minified version.
  	});

  	gulp.task('fe-scripts-controllers', function () {
  		return gulp.src([
  			'public/fe/scripts/controllers/**/*.js',
  			])
  		.pipe(uglify({ mangle: false }).on('error', function(e){
  			console.log(e);
  		}))
  		.pipe(rename(function(path) {
  			path.extname = ".js";
  		}))
  		.pipe(gulp.dest('public/fe/min_scripts/controllers'));
  	});

  	gulp.task('fe-scripts-factory', function () {
  		return gulp.src([
  			'public/fe/scripts/factory/**/*.js',
  			])
  		.pipe(uglify({ mangle: false }).on('error', function(e){
  			console.log(e);
  		}))
  		.pipe(rename(function(path) {
  			path.extname = ".js";
  		}))
  		.pipe(gulp.dest('public/fe/min_scripts/factory'));
  	});
	// gulpSequence - Q-ing of task (para nd sabay sabay)
  	gulp.task('fe-minify-all', gulpSequence('fe-scripts','fe-scripts-controllers','fe-scripts-factory'));
