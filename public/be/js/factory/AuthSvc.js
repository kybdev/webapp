'use strict';
/* Service */
app.service('AuthSvc', function($http, $q, Config, store, $window, jwtHelper, mainFactory, RestAcl){
    var auth = {
        isAuthorized: function(){
            var loggedin = auth.getAuthToken() ? true : false;
            return loggedin;
        },
        getAuthToken: function(){
            return store.get('hc_token');
        },
        getAuthRefreshToken: function(){
            return store.get('hc_rtoken');
        },
        setTokens: function(token, refreshtoken){
            store.set('hc_token', token);
            store.set('hc_rtoken', refreshtoken);
            var json = jwtHelper.decodeToken(token);
            RestAcl.setAcl(json.roles);
        },
        purgeTokens: function(){
            store.remove("hc_token");
            store.remove('hc_rtoken');
            store.remove('hc_lockscreen');
            store.remove('security');
            store.remove('mynav');
            store.remove('order_filter');
            store.remove('activeTab');
            store.remove('order_filter');
        },
        login: function (user, redirect, callback) {
            $http({
                url: "/members/memberlogin",
                method: "POST",
                skipAuthorization: true,
                data: user
            }).success(function (data, status, headers, config) {
                auth.setTokens(data.records.token, data.records.refreshtoken);
                if(redirect) { $window.location.href = "/"; } else {
                    callback(true,data);
                }
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        },
        requestRefreshToken: function(){
            return $http({
                url: "/members/refreshtoken",
                method: "POST",
                skipAuthorization: true,
                data: {'refresh': auth.getAuthRefreshToken()}
            })
        },
        isTokenValid: function(){
            
            if(auth.getAuthToken() != null){
                if (jwtHelper.isTokenExpired(auth.getAuthToken())) {
                    return "expired";
                }else{
                    if(store.get("security") == null){
                        store.set("security", auth.getUser().security);
                    }
                    return "valid";
                }
            }else{
                return "unauthorized";
            }
        },
        execExit: function(exitRoute){
            $window.location.href = exitRoute;
        },
        checkErrorAuth: function(code){
            if (code >=400 && code <=417){
                return true;
            }
            return false;
        },
        getUser: function (){
            if (store.get('hc_token')) {
                var token = store.get('hc_token');
                return jwtHelper.decodeToken(token);
            } else {
                return {};
            }
        },
        getIdentity: function(post, callback){
            $http({
                url: "/members/forgotpassword/identify",
                method: "POST",
                data: post,
                skipAuthorization: true
            }).success(function (data, status, headers, config) {
                callback(true,data);
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        },
        getQuestions: function(callback){
            $http({
                url: "/members/forgotpassword/security_questions",
                method: "GET",
                skipAuthorization: true,
                headers: {
                    "Authorization": "Bearer "+ store.get('fp_token') 
                },
            }).success(function (data, status, headers, config) {
                callback(true,data);
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        },
        fpResetRequest: function(post,callback){
            $http({
                url: "/members/forgotpassword/reset_request",
                method: "POST",
                data: post,
                skipAuthorization: true,
                headers: {
                    "Authorization": "Bearer "+ store.get('fp_token') 
                },
            }).success(function (data, status, headers, config) {
                callback(true,data);
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        },
        fpResetPassword: function(post,callback){
            $http({
                url: "/members/forgotpassword/new_password",
                method: "POST",
                data: post,
                skipAuthorization: true,
                headers: {
                    "Authorization": "Bearer "+ store.get('fp_token') 
                }
            }).success(function (data, status, headers, config) {
                callback(true,data);
            }).error(function (data, status, headers, config) {
                callback(false,data);
            });
        }
    }
    return auth;
});