'use strict';

app.factory('studentFctry', function ($stateParams, $http, $q, Config) {
    return {
        $submitPost: function (postData) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/save",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postData)
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },
        $getStudentList: function (postData) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/list/" + postData.page + "/" + postData.count + "/" + postData.keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postData)
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },
        $getStudent: function (studentId) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/info/"+studentId,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },
        $updatePost: function (postData) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/save",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postData)
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },
        $getRegisterList: function (postData) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/reg/list/" + postData.page + "/" + postData.count + "/" + postData.keyword,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postData)
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },

        $getRegistrant: function (regid) {
            let deferred = $q.defer();
            $http({
                url: Config.ApiURL+ "/student/registrant/"+regid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function (success){
                deferred.resolve(success.data.records);
            },function (error){
                deferred.reject(error.records);
            });
            return deferred.promise;
        },
    }
});
