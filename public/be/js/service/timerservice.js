
app.service('timerService', function($interval,errorService){
	var js = {
		timer: null,
		getTimeRemaining: function(endtime) {
			var t = Date.parse(endtime) - Date.parse(new Date());
			var seconds = Math.floor((t / 1000) % 60);
			var minutes = Math.floor((t / 1000 / 60) % 60);
			var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
			var days = Math.floor(t / (1000 * 60 * 60 * 24));
			  return {
			    'total': t,
			    'days': days,
			    'hours': hours,
			    'minutes': minutes,
			    'seconds': seconds
			  };
		},
		initializeClock: function(endtime) {
				var minute = 0;
				var second = 0;
				var inter;
			  
			  function updateClock() {
			    var t = js.getTimeRemaining(endtime);
			    minute = ('0' + t.minutes).slice(-2);
			    second = ('0' + t.seconds).slice(-2);
			    
			    js.timer = minute + ":" + second;

			    if (t.total <= 0) {
			    	errorService.setError('system', null);
			    	js.timer = "";
		          if (angular.isDefined(inter)) {
		            $interval.cancel(inter);
		            inter = undefined;
		          }
			    }
			  }
			  updateClock();

			  inter = $interval(updateClock, 1000);
		}
	}
	return js;
})