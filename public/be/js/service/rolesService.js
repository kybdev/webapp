'use strict';

app.service('rolesService', function($q, store, jwtHelper, RestAcl, growlService) {

    var rs = {
        _roles : {

            // For Patient Care
            ADMIN : {
                collection : ['pc-admin'],
                errormessage : ''   
            },

            // For Patient Care
            ADMINCMAPSQAM : {
                collection : ['pc-admin', 'pc-cm', 'pc-aps', 'pc-qam'],
                errormessage : 'You are not allowed to create schedules.'   
            },

            // For Patient Care
            ADMINCMAPS : {
                collection : ['pc-admin', 'pc-cm', 'pc-aps'],
                errormessage : 'You are not allowed to create schedules.'   
            },
            //Uploading attachment files
            ADMINUATCMQAM : {
                collection : ['pc-admin', 'pc-uat', 'pc-cm', 'pc-qam'],
                errormessage : 'You are not allowed to upload documents.'
            },

            //Global view for patient
            ADMINCLICMCRHHAMSWQAMTHR : {
                collection : ['pc-admin', 'pc-cli', 'pc-cm', 'pc-cr', 'pc-hha', 'pc-msw', 'pc-qam', 'pc-thr'],
                errormessage : 'You are not allowed to upload documents.'
            },

            //Admin CM only
            ADMINCM : {
                collection : ['pc-admin', 'pc-cm'],
                errormessage : 'Only admins and case managers are allowed to do this.'  
            },

            //Admin QA CM only
            ADMINCMQA : {
                collection : ['pc-admin', 'pc-cm', 'pc-qam'],
                errormessage : 'Only admins and case managers and QA Managers are allowed to do this.'  
            }

        },
        setRoles : function(token){
            var json = jwtHelper.decodeToken(token);
            store.set("user_roles", json.roles);
        },
        getRoles : function(){
            return store.get("user_roles");
        },
        deny : function (indexProp, notify){
            if(rs._roles.hasOwnProperty(indexProp)){
                var aclRole = RestAcl.isOk(rs._roles[indexProp]['collection']);
                if(!aclRole.result){
                    if(notify){
                        growlService.dismiss();
                        growlService.growl(rs._roles[indexProp]['errormessage'], "warning");
                    }
                    return true;
                }
            }
            return false;
        },
        getRoleCollection : function (indexProp){
            if(rs._roles.hasOwnProperty(indexProp)){
                return rs._roles[indexProp]['collection'];
            }
            return false
            console.error("No Role Collection Exists");
        }
    };

    return rs;

})