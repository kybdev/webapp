app.service('fpService', function(fp){
	return {
		getStep: function(){
			return fp.step;
		},
		getAttempts: function(){
			return fp.attempts;
		},
		setStep: function(num){
			fp.step = num;
		},
	};
})