'use strict';
/**
  * controller for Validation Form example
*/
app.controller('registrantEditCtrl', ["$scope", "$state", "$timeout", "SweetAlert", "studentFctry", "$uibModal", "$log", "$stateParams", function ($scope, $state, $timeout, SweetAlert, studentFctry, $uibModal , $log, $stateParams) {


    studentFctry.$getRegistrant($stateParams.pkregister).then(function(dbData){
        $scope.myModel = dbData;
    }, function(error){
        // Error
    })


    $scope.form = {
        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {
                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }
                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;

            } else {
                //your code for submit
                this.submitPost($scope.myModel, form);
            }

        },
        reset: function (form) {
            // $scope.myModel = angular.copy($scope.master);
            // form.$setPristine(true);

        },
        submitPost : function(data, form){
            var dis = this;
            studentFctry.$updatePost(data).then(function(success){
                if(success){
                    SweetAlert.swal("Success!", "Student Record has been Successfuly updated!", "success");

                }
            }, function(error){
                SweetAlert.swal("Something went wrong while updating student record", "error");
            })

        },
        modal :  function(size, template, modelName, formData){
            if(!formData.hasOwnProperty(modelName)){
                formData[modelName] = [];
            }
            var modalInstance = $uibModal.open({
                templateUrl: template,
                size: size,
                controller: function($scope, $uibModalInstance){

                    $scope.addData = function (form, modaData) {
                        var firstError = null;
                        if (form.$invalid) {
                            var field = null, firstError = null;
                            for (field in form) {
                                if (field[0] != '$') {
                                    if (firstError === null && !form[field].$valid) {
                                        firstError = form[field].$name;
                                    }
                                    if (form[field].$pristine) {
                                        form[field].$dirty = true;
                                    }
                                }
                            }
                            angular.element('.ng-invalid[name=' + firstError + ']').focus();
                            return;

                        } else {
                            formData[modelName].push(modaData);
                            $uibModalInstance.close(formData);
                        }
                    };

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });
            modalInstance.result.then(function (data) {
                console.log(data);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        },

        modalEdit :  function(size, template, formData){
            var modalInstance = $uibModal.open({
                templateUrl: template,
                size: size,
                controller: function($scope, $uibModalInstance){
                    $scope.myModel = formData;
                    $scope.addData = function (form, modaData) {
                        $uibModalInstance.close(formData);
                    };
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                console.log(data);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        },

        removeList:  function(index, data){
            data.splice(index, 1);
        },

        upload :  function(size, template, modelName, formData){

            var modalInstance = $uibModal.open({
                templateUrl: template,
                size: size,
                controller: function($scope, $uibModalInstance){
                    $scope.myImage = '';
                    $scope.myCroppedImage = '';
                    $scope.cropType = "square";


                    $scope.setImage = function(){


                        formData[modelName] = $scope.myCroppedImage;
                        $uibModalInstance.close();
                    }

                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }

            });
            modalInstance.result.then(function (data) {
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        },

    };










}]);
