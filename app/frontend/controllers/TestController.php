<?php

namespace Modules\Frontend\Controllers;

use \Phalcon\Mvc\View;

class TestController extends ControllerBase
{

    public function onConstruct(){

    }

    public function indexAction(){
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function actionAction($params){
        $this->view->params = $params;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
