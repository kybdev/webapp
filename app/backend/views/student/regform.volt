<!-- start: PAGE TITLE -->
<section id="page-title">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Registrant Information</h1>
			<span class="mainDescription">[Some Details Here]</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->

<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-md-12">
			<form name="Form" id="form1" novalidate ng-submit="form.submit(Form)" method="post">

				<div class="row">
					<div class="col-md-12">
						<?php echo $this->view->getRender('student/register', 'personalDetail');?>
					</div>
				</div>

			

				<!-- <div class="row">
					<div class="col-md-12">
						<div class="pull-right">
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
							<button type="reset" class="btn btn-primary btn-o" ng-click="form.reset(Form)">
								Reset
							</button>
						</div>
					</div>
				</div> -->
			</form>
		</div>
	</div>
</div>


<script type="text/ng-template" id="children.html">
    <?php echo $this->view->getRender('student/modal', 'children');?>
</script>

<script type="text/ng-template" id="educBackground.html">
    <?php echo $this->view->getRender('student/modal', 'educBackground');?>
</script>

<script type="text/ng-template" id="milBackground.html">
    <?php echo $this->view->getRender('student/modal', 'milBackground');?>
</script>

<script type="text/ng-template" id="upload.html">
    <?php echo $this->view->getRender('student/modal', 'upload');?>
</script>
