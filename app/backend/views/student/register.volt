<!-- start: PAGE TITLE -->
<section id="page-title">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Registered Student List</h1>
			{#<span class="mainDescription">This directive allow to liven your tables. It support sorting, filtering and pagination. Header row with titles and filters automatic generated on compilation step.</span>#}
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: TABLE WITH SORTING -->
<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-md-6 search form-group" >
			<div class="input-group">
				<span class="zmdi icon input-group-addon ti-search"></span>

				<input type="text" class="search-field form-control" placeholder="Search" ng-model="keyword" ng-keypress="submitsearch($event)">
			</div>
		</div>


		<div class="col-md-12">
			<div>
				<table class="table table-hover" id="sample-table-1">
			        <thead>
			            <tr>
			                <th>AFPSN </th>
			                <th>AFPID No </th>
			                <th>Rank  </th>
			                <th>Full Name </th>
							<th>Religion</th>
							<th>Contact </th>
							<th>Email </th>
							<th></th>
			            </tr>
			        </thead>
			        <tbody>
			            <tr ng-repeat="list in dataList">
			                <td class=""><span ng-bind-html="list['AFPSN']"></span></td>
			                <td class=""><span ng-bind-html="list['AFPID No']"></span></td>
			                <td class=""><span ng-bind-html="list['Rank']"></span></td>
			                <td class=""><span ng-bind-html="list['Name']"></span></td>
							<td class=""><span ng-bind-html="list['Religion']"></span></td>
							<td class=""><span ng-bind-html="list['Contact']"></span></td>
							<td class=""><span ng-bind-html="list['Email']"></span></td>

			                <td class="center">
			                    <div class="visible-md visible-lg hidden-sm hidden-xs">
			                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ui-sref="app.studentRegistrant({ 'pkregister': list.pkregister })"><i class="fa fa-pencil"></i></a>
			                        {#<a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.importUnitASsignment)"><i class="fa fa-times fa fa-white"></i></a>#}
			                    </div>
			                </td>
			            </tr>
			        </tbody>
			    </table>

				<div id="data-table-command-footer" class="bootgrid-footer container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            {#<uib-pagination total-items="listTotal"  ng-model="currentpage" max-size="pagesize" items-per-page="list" class="pagination-sm" boundary-links="true" ng-click="setPage(currentpage)" ng-hide="listTotal <= list" ></uib-pagination>                                </div>#}

							<ul uib-pagination total-items="listTotal" ng-model="currentpage" max-size="pagesize" items-per-page="list" class="pagination-sm" boundary-link-numbers="true" rotate="false" ng-click="setPage(currentpage)"></ul>

                            <div class="col-sm-6 ">
                                <div class="infos" pagination-items total="listTotal" page="page" limit="list"></div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<!-- end: TABLE WITH SORTING -->
