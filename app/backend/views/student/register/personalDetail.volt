<div class="row">
    <!-- <div class="col-md-4">
        <fieldset>
            <legend>
                Upload Photo
            </legend>
            <div class="row">

                <input hidden="true" ng-model="myModel.temp" ng-init="myModel.temp = 'default'" />
                <div class="col-xs-6 col-xs-offset-3">
                    <img src="{[{myModel.profile ? myModel.profile : 'be/images/default.jpg' }]}" class="img-rounded img-responsive margin-bottom-15" alt="A generic image with rounded corners">
                        <a class="btn btn-wide btn-primary" ng-click="form.upload('lg', 'upload.html', 'profile', myModel); myModel.temp">
                            <i class="glyphicon glyphicon-share"></i>
                        </a>
                </div>


            </div>
        </fieldset>
    </div> -->
    <div class="col-md-4">
        <div class="form-group" ng-class="{'has-error':Form.fname.$dirty && Form.fname.$invalid, 'has-success':Form.fname.$valid}">
            <label class="control-label">
                First Name <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter  First Name" class="form-control" name="fname" ng-model="myModel.fname" required />
            <span class="error text-small block" ng-if="Form.fname.$dirty && Form.fname.$invalid">First Name is required</span>
            
        </div>

        <div class="form-group" ng-class="{'has-error':Form.mname.$dirty && Form.mname.$invalid, 'has-success':Form.mname.$valid}">
            <label class="control-label">
                Middle Name <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter  Middle Name" class="form-control" name="mname" ng-model="myModel.mname"  />
            <span class="error text-small block" ng-if="Form.mname.$dirty && Form.mname.$invalid">Middle Name is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.lname.$dirty && Form.lname.$invalid, 'has-success':Form.lname.$valid}">
            <label class="control-label">
                Last Name <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter  Last Name" class="form-control" name="lname" ng-model="myModel.lname" required />
            <span class="error text-small block" ng-if="Form.lname.$dirty && Form.lname.$invalid">Last Name is required</span>
            
        </div>

        <div class="form-group" ng-class="{'has-error':Form.rank.$dirty && Form.rank.$invalid, 'has-success':Form.rank.$valid}">
            <label class="control-label">
                Rank <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Rank" class="form-control" name="rank" ng-model="myModel.rank" required />
            <span class="error text-small block" ng-if="Form.rank.$dirty && Form.rank.$invalid">Last Name is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.afpSn.$dirty && Form.afpSn.$invalid, 'has-success':Form.afpSn.$valid}">
            <label class="control-label">
                AFPSN <span class="symbol required"></span> 
            </label>
            <input type="text" placeholder="Enter AFPSN" class="form-control" name="afpSn" ng-model="myModel.afpSn" required />
            <span class="error text-small block" ng-if="Form.afpSn.$dirty && Form.afpSn.$invalid">Last Name is required</span>
           
        </div>

        <div class="form-group" ng-class="{'has-error':Form.afpIdno.$dirty && Form.afpIdno.$invalid, 'has-success':Form.afpIdno.$valid}">
            <label class="control-label">
                AFP ID No. <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="afpIdno" ng-model="myModel.afpIdno" required />
            <span class="error text-small block" ng-if="Form.afpIdno.$dirty && Form.afpIdno.$invalid">Last Name is required</span>
        </div>


    </div>

    <div class="col-md-4">
        

        <div class="form-group" ng-class="{'has-error':Form.dateOfBirth.$dirty && Form.dateOfBirth.$invalid, 'has-success':Form.dateOfBirth.$valid}">
            <label class="control-label">
                Date of Birth <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="dateOfBirth" ng-model="myModel.dateOfBirth" required />
            <span class="error text-small block" ng-if="Form.dateOfBirth.$dirty && Form.dateOfBirth.$invalid">Date of Birth is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.placeOfBirth.$dirty && Form.placeOfBirth.$invalid, 'has-success':Form.placeOfBirth.$valid}">
            <label class="control-label">
                Place of Birth <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="placeOfBirth" ng-model="myModel.placeOfBirth" required />
            <span class="error text-small block" ng-if="Form.placeOfBirth.$dirty && Form.placeOfBirth.$invalid">Place of Birth is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.religion.$dirty && Form.religion.$invalid, 'has-success':Form.religion.$valid}">
            <label class="control-label">
                Religion <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="religion" ng-model="myModel.religion" required />
            <span class="error text-small block" ng-if="Form.religion.$dirty && Form.religion.$invalid">Religion is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.contact.$dirty && Form.contact.$invalid, 'has-success':Form.contact.$valid}">
            <label class="control-label">
                Contact No. <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="contact" ng-model="myModel.contact" required />
            <span class="error text-small block" ng-if="Form.contact.$dirty && Form.contact.$invalid">Contact No. is required</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.email.$dirty && Form.email.$invalid, 'has-success':Form.email.$valid}">
            <label class="control-label">
                E-Mail <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter AFP ID No." class="form-control" name="email" ng-model="myModel.email" required />
            <span class="error text-small block" ng-if="Form.email.$dirty && Form.email.$invalid"> E-Mail is required</span>
        </div>





    </div>


</div>

