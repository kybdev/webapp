<!-- start: PAGE TITLE -->
<section id="page-title">
	<div class="row">
		<div class="col-sm-8">
			<h1 class="mainTitle" >Add User</h1>
			<span class="mainDescription">[Some Details Here]</span>
		</div>
		<div ncy-breadcrumb></div>
	</div>
</section>
<!-- end: PAGE TITLE -->
<!-- start: FORM VALIDATION -->
<div class="container-fluid container-fullw bg-white">
	<div class="row">
		<div class="col-md-12">
			<div >
				<form name="Form" id="form1" novalidate ng-submit="form.submit(Form)" method="post">
					<div class="row">
						<div class="col-md-6">
							<fieldset>
								<legend>
									New User personal information
								</legend>
								<div class="form-group" ng-class="{'has-error':Form.firstName.$dirty && Form.firstName.$invalid, 'has-success':Form.firstName.$valid}">
									<label class="control-label">
										First Name <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Enter your First Name" class="form-control" name="firstName" ng-model="myModel.firstName" required />
									<span class="error text-small block" ng-if="Form.firstName.$dirty && Form.firstName.$invalid">First Name is required</span>
									<span class="success text-small" ng-if="Form.firstName.$valid">Thank You!</span>
								</div>

								<div class="form-group" >
									<label class="control-label">
										Middle Name
										{#<span class="symbol required"></span>#}
									</label>
									<input type="text" placeholder="Enter your Middle Name" class="form-control" name="middleName" ng-model="myModel.middleName"  />
									<span class="error text-small block" ng-if="Form.middleName.$dirty && Form.middleName.$invalid">Middle Name is required</span>
									{#<span class="success text-small" ng-if="Form.middleName.$valid">Thank You!</span>#}
								</div>

								<div class="form-group" ng-class="{'has-error':Form.lastName.$dirty && Form.lastName.$invalid, 'has-success':Form.lastName.$valid}">
									<label class="control-label">
										Last Name <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Enter your Last Name" class="form-control" name="lastName" ng-model="myModel.lastName" required />
									<span class="error text-small block" ng-if="Form.lastName.$dirty && Form.lastName.$invalid">Last Name is required</span>
									<span class="success text-small" ng-if="Form.lastName.$valid">Wonderful!</span>
								</div>

								<div class="form-group" ng-class="{'has-error':Form.gender.$dirty && Form.gender.$invalid, 'has-success':Form.gender.$valid}">
									<label class="control-label block">
										Gender <span class="symbol required"></span>
									</label>
									<div class="clip-radio radio-primary">
										<input type="radio" id="wd-female" name="gender" value="female" ng-model="myModel.gender" required>
										<label for="wd-female">
											Female
										</label>
										<input type="radio" id="wd-male" name="gender" value="male" ng-model="myModel.gender" required>
										<label for="wd-male">
											Male
										</label>
									</div>
									<span class="error text-small block" ng-if="Form.gender.$dirty && Form.gender.$error.required">Gender is required.</span>
									<span class="success text-small block" ng-if="Form.gender.$valid && form.gender == 'male'">Thank You, Mr. {[{form.lastName}]}!</span>
									<span class="success text-small block" ng-if="Form.gender.$valid && form.gender == 'female'">Thank You, Mrs. {[{form.lastName}]}!</span>
								</div>

								<div class="form-group" ng-class="{'has-error':Form.birthdate.$dirty && Form.birthdate.$invalid, 'has-success':Form.birthdate.$valid}">
									<label class="control-label">
									Birth Date<span class="symbol required"></span>
									</label>
									<input type="text" class="form-control" name="birthdate" ng-model="myModel.birthdate" placeholder="99/99/9999" ui-mask="99/99/9999" required>
									<span class="error text-small block" ng-if="Form.birthdate.$dirty && Form.birthdate.$error.required">Birth Date is required.</span>
								</div>


								<div class="form-group" ng-class="{'has-error':Form.mobile.$dirty && Form.mobile.$invalid, 'has-success':Form.mobile.$valid}">
									<label class="control-label">
									Contact Number<span class="symbol required"></span>
									</label>
									<input type="text" placeholder="(999) 999-9999" class="form-control" name="mobile" ng-model="myModel.mobile" ui-mask="(999) 999-9999" required>
									<span class="error text-small block" ng-if="Form.mobile.$dirty && Form.mobile.$error.required">Contact is required.</span>
									<span class="error text-small block" ng-if="Form.email.$error.mobile">Please, enter a valid contact Number.</span>
								</div>

								<div class="form-group" ng-class="{'has-error':Form.email.$dirty && Form.email.$invalid, 'has-success':Form.email.$valid}">
									<label class="control-label">
										Email <span class="symbol required"></span>
									</label>
									<input type="email" placeholder="Enter a valid E-mail" class="form-control" name="email" ng-model="myModel.email" required>
									<span class="error text-small block" ng-if="Form.email.$dirty && Form.email.$error.required">Email is required.</span>
									<span class="error text-small block" ng-if="Form.email.$error.email">Please, enter a valid email address.</span>
									<span class="success text-small" ng-if="Form.email.$valid">It's a valid e-mail!</span>
								</div>

							</fieldset>

						</div>
						<div class="col-md-6">
							<fieldset>
								<legend>
									More Information
								</legend>

								<div class="form-group" ng-class="{'has-error':Form.serialNumber.$dirty && Form.serialNumber.$invalid, 'has-success':Form.serialNumber.$valid}">
									<label class="control-label">
										Serial Number <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Enter your Last Name" class="form-control" name="serialNumber" ng-model="myModel.serialNumber" required />
									<span class="error text-small block" ng-if="Form.serialNumber.$dirty && Form.serialNumber.$invalid">Serial Number is required</span>
									<span class="success text-small" ng-if="Form.serialNumber.$valid">Thank You!</span>
								</div>

								<div class="form-group" ng-class="{'has-error':Form.rank.$dirty && Form.rank.$invalid, 'has-success':Form.rank.$valid}">
									<label class="control-label">
										Rank <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Enter your Rank" class="form-control" name="rank" ng-model="myModel.rank" required />
									<span class="error text-small block" ng-if="Form.rank.$dirty && Form.rank.$invalid">Rank is required</span>
									<span class="success text-small" ng-if="Form.rank.$valid">Thank You!</span>
								</div>


								<div class="form-group" ng-class="{'has-error':Form.designation.$dirty && Form.designation.$invalid, 'has-success':Form.designation.$valid}">
									<label class="control-label">
										Designation <span class="symbol required"></span>
									</label>
									<input type="text" placeholder="Enter your Rank" class="form-control" name="designation" ng-model="myModel.designation" required />
									<span class="error text-small block" ng-if="Form.designation.$dirty && Form.designation.$invalid">Designation is required</span>
									<span class="success text-small" ng-if="Form.designation.$valid">Thank You!</span>
								</div>

							</fieldset>
						</div>

						<div class="col-md-6">
							<fieldset>
								<legend>
									Account
								</legend>

								<div class="form-group" ng-class="{'has-error':Form.accountType.$dirty && Form.accountType.$invalid, 'has-success':Form.accountType.$valid}">
									<label class="control-label block">
										Account Type <span class="symbol required"></span>
									</label>
									<div class="clip-radio radio-primary">
										<input type="radio" id="wd-admin" name="accountType" value="admin" ng-model="myModel.accountType" required>
										<label for="wd-admin">
											Admin
										</label>
										<input type="radio" id="wd-guess" name="accountType" value="guess" ng-model="myModel.accountType" required>
										<label for="wd-guess">
											Guess
										</label>
									</div>
									<span class="error text-small block" ng-if="Form.accountType.$dirty && Form.accountType.$error.required">Account Type is required.</span>
								</div>


							</fieldset>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pull-right">
								<button type="submit" class="btn btn-primary">
									Submit
								</button>
								<button type="reset" class="btn btn-primary btn-o" ng-click="form.reset(Form)">
									Reset
								</button>
							</div>
						</div>
					</div>
					{#<pre class="margin-top-20">{[{ myModel | json }]}</pre>#}
				</form>
			</div>
		</div>
	</div>
</div>
<!-- end: FORM VALIDATION -->
