<?php
namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class SystemController extends ControllerBase
{

    public function maintenanceAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function audittrailAction($params){
        $this->view->params = $params;
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
