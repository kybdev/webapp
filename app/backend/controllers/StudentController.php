<?php
namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class StudentController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function addAction($params){
        $this->view->params = $params;
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function registerAction($params){
        $this->view->params = $params;
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function regformAction($params){
        $this->view->params = $params;
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    
}
