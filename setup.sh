#!/bin/sh

config="dist/config/config.$1.php";

if [ ! -f $config ]; then
    printf "File not found! "$config
    exit
fi

copyconfig=$(dirname $(readlink -f $0))"/$config"
destconfig=$(dirname $(readlink -f $0))"/app/config/config.php"

if [ -f $destconfig ]; then
    sudo rm $destconfig
fi

sudo cp $copyconfig $destconfig

printf "Your configuration '$copyconfig' has been setup.\n\n";

